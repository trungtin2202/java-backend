//package com.example.demo;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.jdbc.core.JdbcTemplate;
//@Slf4j
//@SpringBootApplication
//public class Database implements CommandLineRunner {
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    public static void main(String[] args) {
//        SpringApplication.run(Database.class, args);
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        log.info("INIT DATABASE");
//        String sql = "create table animals (\n" +
//                "        id bigint not null,\n" +
//                "        address varchar(255),\n" +
//                "        age integer,\n" +
//                "        name varchar(255),\n" +
//                "        primary key (id)\n" +
//                "    )";
//        jdbcTemplate.execute(sql);
////        String sql = "INSERT INTO students (name, email) VALUES ("
////                + "'Nam Ha Minh', 'nam@codejava.net')";
//
////        int rows = jdbcTemplate.update(sql);
////        if (rows > 0) {
////            System.out.println("A new row has been inserted.");
////        }
//    }
//
//}