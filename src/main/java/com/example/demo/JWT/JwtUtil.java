package com.example.demo.JWT;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.HashMap;
import java.util.Map;

public class JwtUtil {

    public static String SECRECT = "Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=";
    public static String getJwt(String username, String createAt, Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("createAt", createAt);
        map.put("id", id);

        return Jwts.builder()
                .setSubject("GreetingClient") // client's identifier
                .setClaims(map)
                .signWith(SignatureAlgorithm.HS256, SECRECT)
                .compact();
    }

    public static Boolean verifyToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRECT).parseClaimsJws(token);
            return true;
        }catch (Exception e) {
            return  false;
        }
    }
}
