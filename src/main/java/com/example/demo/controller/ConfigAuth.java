package com.example.demo.controller;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.dto.AuthRequest;
import com.example.demo.service.AuthInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ConfigAuth {
    @Autowired
    AuthInterface authServices;

    @GetMapping("/auth")
    public ResponseEntity<ResponseCommon> getListAuth() {
        return authServices.getListAuth();
    }

    @PostMapping("/auth")
    public ResponseEntity<ResponseCommon> saveAuth(@RequestBody AuthRequest bodyData) {
        return authServices.saveAuth(bodyData);
    }

    @PutMapping("/auth/{id}")
    public ResponseEntity<ResponseCommon> editAuth(@PathVariable Long id, @RequestBody AuthRequest bodyData) {
        return authServices.editAuth(id, bodyData);
    }

    @DeleteMapping("/auth/{id}")
    public ResponseEntity<ResponseCommon> deleteAuth(@PathVariable Long id) {
        return authServices.deleteAuth(id);
    }
}
