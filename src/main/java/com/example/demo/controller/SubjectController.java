package com.example.demo.controller;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.entity.SubjectModels;
import com.example.demo.service.SubjectInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class SubjectController {
    @Autowired
    private SubjectInterface subjectInterface;

    @GetMapping("/subject")
    public ResponseEntity<ResponseCommon> getSubject() {
        return subjectInterface.getSubject();
    }
    @GetMapping("/subject/{id}")
    public ResponseEntity<ResponseCommon> getDetailSubject(@PathVariable String id) {
        return subjectInterface.getDetailSubject(id);
    }
    @PostMapping("/subject")
    public ResponseEntity<ResponseCommon> saveSubject(@RequestBody SubjectModels bodyData) {
        return subjectInterface.saveSubject(bodyData);
    }
    @PutMapping("/subject/{id}")
    public ResponseEntity<ResponseCommon> editSubject(@PathVariable String id,@RequestBody SubjectModels bodyData) {
        return subjectInterface.editSubject(id,bodyData);
    }
    @DeleteMapping("/subject/{id}")
    public ResponseEntity<ResponseCommon> deleteSubject(@PathVariable String id) {
        return subjectInterface.deleteSubject(id);
    }
}
