package com.example.demo.controller;

import com.example.demo.dto.LoginRequest;
import com.example.demo.dto.UserRequest;
import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.service.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    private UserInterface userServices;

    @PostMapping("/register")
    public ResponseEntity<ResponseCommon> register(@RequestBody UserRequest userRequest) {
        return userServices.register(userRequest);
    }

    @GetMapping("/users")
    public ResponseEntity<ResponseCommon> getUsers() {
        return userServices.getUser();
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseCommon> login(@RequestBody LoginRequest loginRequest) {
        return userServices.login(loginRequest);
    }
    @GetMapping("/students")
    public ResponseEntity<ResponseCommon> getStudentList() {
        return userServices.getStudentList();
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<ResponseCommon> getDetailStudent(@PathVariable Long id) {
        return userServices.getDetailStudent(id);
    }

    @GetMapping("/teachers")
    public ResponseEntity<ResponseCommon> getTeacherList() {
        return userServices.getTeacherList();
    }

    @GetMapping("/teachers/{id}")
    public ResponseEntity<ResponseCommon> getDetailTeacher(@PathVariable Long id) {
        return userServices.getDetailTeacher(id);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<ResponseCommon> editUser(@RequestBody UserRequest userRequest, @PathVariable Long id) {
        return userServices.editUser(userRequest, id);
    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity<ResponseCommon> deleteUser(@PathVariable Long id) {
        return userServices.deleteUser(id);
    }
}
