package com.example.demo.controller.common;

import lombok.Data;

import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class ExceptionCommon extends Exception{
    Integer code;
    String message;
    String exception;
}