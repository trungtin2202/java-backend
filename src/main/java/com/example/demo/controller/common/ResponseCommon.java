package com.example.demo.controller.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseCommon {
    Integer code;
    String message;
    Object data;
}
