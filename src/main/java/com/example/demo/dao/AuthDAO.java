package com.example.demo.dao;

import com.example.demo.entity.AuthModels;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuthDAO extends JpaRepository<AuthModels, Long> {
    boolean existsByRouterLink(String RouterLink);
}
