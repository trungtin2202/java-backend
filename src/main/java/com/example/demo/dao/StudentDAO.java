package com.example.demo.dao;

import com.example.demo.entity.StudentModels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StudentDAO extends JpaRepository<StudentModels, Long> {
    @Query(nativeQuery = true, value = "select * from students u where u.user_id = :user_id")
    Optional<StudentModels> getStudentByUserId(@Param("user_id") Long user_id);
}