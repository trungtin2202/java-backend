package com.example.demo.dao;

import com.example.demo.entity.SubjectModels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface SubjectDAO extends JpaRepository<SubjectModels, String> {
    @Query(nativeQuery = true, value="select * from subjects s where s.subject_code = :subjectCode")
    Optional<SubjectModels> findBySubjectCode(@Param("subjectCode") String subjectCode);

    boolean existsBySubjectCode(String subjectCode);
}
