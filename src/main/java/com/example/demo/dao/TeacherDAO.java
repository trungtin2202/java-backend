package com.example.demo.dao;

import com.example.demo.entity.TeacherModels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface TeacherDAO extends JpaRepository<TeacherModels, Long> {
    @Query(nativeQuery = true, value = "select * from teacher u where u.user_id = :user_id")
    Optional<TeacherModels> getTeacherByUserId(@Param("user_id") Long user_id);
}