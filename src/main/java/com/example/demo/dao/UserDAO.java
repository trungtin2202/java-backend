package com.example.demo.dao;

import com.example.demo.entity.UserModels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface UserDAO extends JpaRepository<UserModels, Long> {
    Boolean existsByUsername(String username);

    @Query(nativeQuery = true, value = "select * from users u where u.username = :username")
    Boolean isExistByUsername(@Param("username") String username);

    @Query(nativeQuery = true, value = "select count (*) from users u where u.username = :username and u.password = :password")
    int loginWithUsernameAndPassowrd(@Param("username") String username, @Param("password") String password);
}