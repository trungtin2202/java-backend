package com.example.demo.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class AuthRequest {
    @NonNull String routerName;
    @NonNull String routerLink;
}
