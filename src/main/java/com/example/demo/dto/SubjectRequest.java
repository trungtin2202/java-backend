package com.example.demo.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class SubjectRequest {
    @NonNull String subjectCode;
    @NonNull String subjectName;
    @NonNull String subjectHour;
}
