package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Builder
@Data
public class UserRequest {
    @NonNull String username;
    @NonNull String password;
    @NonNull String email;
    String phoneNumber;
    String fullName;
    String type;
}
