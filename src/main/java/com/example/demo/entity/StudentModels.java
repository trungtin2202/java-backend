package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "students")
public class StudentModels{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @NonNull String username;
    @NonNull String password;
    @NonNull String email;
    String phoneNumber;
    String fullName;
    Date createAt;
    String type; /// Student
    @Column(unique=true)
    private Long userId;
}
