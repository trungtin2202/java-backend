package com.example.demo.entity;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Table(name = "subjects")
@NoArgsConstructor
public class SubjectModels {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @NonNull String subjectCode;
    @NonNull String subjectName;
    @NonNull Integer subjectHour;
}
