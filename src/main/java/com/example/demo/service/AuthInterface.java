package com.example.demo.service;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.dto.AuthRequest;
import org.springframework.http.ResponseEntity;

public interface AuthInterface {
    ResponseEntity<ResponseCommon> getListAuth();
    ResponseEntity<ResponseCommon> saveAuth(AuthRequest bodyData);
    ResponseEntity<ResponseCommon> editAuth(Long id, AuthRequest bodyData);
    ResponseEntity<ResponseCommon> deleteAuth(Long id);
}
