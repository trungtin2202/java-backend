package com.example.demo.service;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.entity.SubjectModels;
import org.springframework.http.ResponseEntity;

public interface SubjectInterface {
    ResponseEntity<ResponseCommon> getSubject();
    ResponseEntity<ResponseCommon> getDetailSubject(String id);
    ResponseEntity<ResponseCommon> saveSubject(SubjectModels bodyData);
    ResponseEntity<ResponseCommon> editSubject(String id, SubjectModels bodyData);
    ResponseEntity<ResponseCommon> deleteSubject(String id);
}
