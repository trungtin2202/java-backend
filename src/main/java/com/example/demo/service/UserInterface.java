package com.example.demo.service;

import com.example.demo.dto.LoginRequest;
import com.example.demo.dto.UserRequest;
import com.example.demo.controller.common.ResponseCommon;
import org.springframework.http.ResponseEntity;

public interface UserInterface {
    ResponseEntity<ResponseCommon> register(UserRequest requestBody);

    ResponseEntity<ResponseCommon> getUser();
    ResponseEntity<ResponseCommon> login(LoginRequest loginRequest);


    ResponseEntity<ResponseCommon> getStudentList();
    ResponseEntity<ResponseCommon> getDetailStudent(Long id);
    ResponseEntity<ResponseCommon> getTeacherList();
    ResponseEntity<ResponseCommon> getDetailTeacher(Long id);
    ResponseEntity<ResponseCommon> editUser(UserRequest userRequest, Long id);
    ResponseEntity<ResponseCommon> deleteUser(Long id);

}
