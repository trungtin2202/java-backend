package com.example.demo.service.impl;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.dao.AuthDAO;
import com.example.demo.dao.SubjectDAO;
import com.example.demo.dto.AuthRequest;
import com.example.demo.entity.AuthModels;
import com.example.demo.entity.SubjectModels;
import com.example.demo.service.AuthInterface;
import com.example.demo.service.SubjectInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class AuthConfigImpl implements AuthInterface{
    private final String TAG = "AUTH CONFIG SERVICES";
    @Autowired
    AuthDAO authDAO;

    @Override
    public ResponseEntity<ResponseCommon> getListAuth() {
        List<AuthModels> authRouter = authDAO.findAll();
        ResponseCommon response = new ResponseCommon(200, "Success", authRouter);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @Override
    public ResponseEntity<ResponseCommon> saveAuth(AuthRequest bodyData) {
        try {
            boolean isExistedRouter = authDAO.existsByRouterLink(bodyData.getRouterLink());
            if (isExistedRouter) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Router already have", null));
            }
            AuthModels auth = new AuthModels();
            auth.setRouterLink(bodyData.getRouterLink());
            auth.setRouterName(bodyData.getRouterLink());
            authDAO.save(auth);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseCommon(200, "Create auth router jwt success", null));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "something went wrong", null));
        }
    }

    @Override
    public ResponseEntity<ResponseCommon> editAuth(Long id, AuthRequest bodyData) {
        Optional<AuthModels> updateSubject = authDAO.findById(id);
        if (updateSubject.isPresent()) {
            AuthModels auth = new AuthModels();
            auth.setId(id);
            auth.setRouterLink(bodyData.getRouterLink());
            auth.setRouterName(bodyData.getRouterName());
            AuthModels saveAuthConfig = authDAO.save(auth);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(200, "Create subject success", saveAuthConfig)
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseCommon(400, "Could not found subject with id: " + id, null)
        );
    }

    @Override
    public ResponseEntity<ResponseCommon> deleteAuth(Long id) {
        Optional<AuthModels> deleteRouter = authDAO.findById(id);
        if (deleteRouter.isPresent()) {
            authDAO.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(200, "Delete success router id: " + id, null)
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseCommon(200, "Could not found router id: " + id, null)
        );
    }
}
