package com.example.demo.service.impl;

import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.entity.SubjectModels;
import com.example.demo.dao.SubjectDAO;
import com.example.demo.service.SubjectInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class SubjectImpl implements SubjectInterface {
    private final String TAG = "SUBJECT SERVICES";
    @Autowired
    SubjectDAO subjectDAO;

    @Override
    public ResponseEntity<ResponseCommon> getSubject() {
        List<SubjectModels> subjects = subjectDAO.findAll();
        ResponseCommon response = new ResponseCommon(200, "Success", subjects);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @Override
    public ResponseEntity<ResponseCommon> getDetailSubject(String subjectCode) {
        Optional<SubjectModels> subject = subjectDAO.findBySubjectCode(subjectCode);
        if (subject.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseCommon(200, "Success", subject));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Could not found subject " + subjectCode, null));
    }

    @Override
    public ResponseEntity<ResponseCommon> saveSubject(SubjectModels bodyData) {
        try {
            boolean isExistedSubject = subjectDAO.existsBySubjectCode(bodyData.getSubjectCode());
            if (isExistedSubject) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Subject already have", null));
            }
            SubjectModels subject = new SubjectModels();
            subject.setSubjectCode(bodyData.getSubjectCode());
            subject.setSubjectName(bodyData.getSubjectName());
            subject.setSubjectHour(bodyData.getSubjectHour());
            SubjectModels saveSubject = subjectDAO.save(subject);
            HashMap<String, Long> id = new HashMap<>();
            id.put("id", saveSubject.getId());
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseCommon(200, "Create subject success", id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "something went wrong", null));
        }
    }

    @Override
    public ResponseEntity<ResponseCommon> editSubject(String id, SubjectModels bodyData) {
        Optional<SubjectModels> updateSubject = subjectDAO.findById(id);
        if (updateSubject.isPresent()) {
            SubjectModels subject = new SubjectModels();
            subject.setSubjectCode(id);
            subject.setSubjectName(bodyData.getSubjectName());
            subject.setSubjectHour(bodyData.getSubjectHour());
            SubjectModels saveSubject = subjectDAO.save(subject);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(200, "Create subject success", saveSubject)
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseCommon(400, "Could not found subject with id: " + id, null)
        );
    }

    @Override
    public ResponseEntity<ResponseCommon> deleteSubject(String id) {
        Optional<SubjectModels> deleteSubject = subjectDAO.findById(id);
        if (deleteSubject.isPresent()) {
            subjectDAO.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(200, "Delete success subject id: " + id, null)
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseCommon(200, "Could not found subject id: " + id, null)
        );
    }
}
