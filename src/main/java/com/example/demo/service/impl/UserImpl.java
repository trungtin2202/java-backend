package com.example.demo.service.impl;

import com.example.demo.dto.LoginRequest;
import com.example.demo.dto.UserRequest;
import com.example.demo.controller.common.ResponseCommon;
import com.example.demo.entity.StudentModels;
import com.example.demo.entity.SubjectModels;
import com.example.demo.entity.TeacherModels;
import com.example.demo.entity.UserModels;
import com.example.demo.dao.StudentDAO;
import com.example.demo.dao.TeacherDAO;
import com.example.demo.dao.UserDAO;
import com.example.demo.service.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserImpl implements UserInterface {
    @Autowired
    UserDAO userDAO;

    @Autowired
    TeacherDAO teacherDAO;

    @Autowired
    StudentDAO studentDAO;

    @Override
    public ResponseEntity<ResponseCommon> register(UserRequest requestBody) {
        try {
            Boolean isExist = userDAO.existsByUsername(requestBody.getUsername());
            if (isExist) {
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseCommon(400, "user already, please login", null)
                );
            }
            Date date = new Date();
            UserModels user = new UserModels();
            user.setEmail(requestBody.getEmail());
            user.setUsername(requestBody.getUsername());

            user.setPassword(requestBody.getPassword());
            user.setPhoneNumber(requestBody.getPhoneNumber());
            user.setFullName(requestBody.getFullName());
            user.setCreateAt(date);
            user.setType(requestBody.getType());
            UserModels newUser = userDAO.save(user);
            if (requestBody.getType().matches("student")) {
                StudentModels student = getStudentModels(requestBody, date, newUser);
                studentDAO.save(student);
            } else {
                TeacherModels teacher = getTeacherModels(requestBody, date, newUser);
                teacherDAO.save(teacher);
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(201, "Sign up success", user)
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Something went wrong", null));
        }
    }

    private static TeacherModels getTeacherModels(UserRequest requestBody, Date date, UserModels newUser) {
        TeacherModels teacher = new TeacherModels();
        teacher.setType("teacher");
        teacher.setEmail(requestBody.getEmail());
        teacher.setUsername(requestBody.getUsername());
        teacher.setPassword(requestBody.getPassword());
        teacher.setPhoneNumber(requestBody.getPhoneNumber());
        teacher.setFullName(requestBody.getFullName());
        teacher.setCreateAt(date);
        teacher.setUserId(newUser.getId());
        return teacher;
    }

    private static StudentModels getStudentModels(UserRequest requestBody, Date date, UserModels newUser) {
        StudentModels student = new StudentModels();
        student.setType("student");
        student.setEmail(requestBody.getEmail());
        student.setUsername(requestBody.getUsername());
        student.setPassword(requestBody.getPassword());
        student.setPhoneNumber(requestBody.getPhoneNumber());
        student.setFullName(requestBody.getFullName());
        student.setCreateAt(date);
        student.setUserId(newUser.getId());
        return student;
    }

    @Override
    public ResponseEntity<ResponseCommon> getUser() {
        try {
            List<UserModels> users = userDAO.findAll();
            if (!users.isEmpty()) {
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseCommon(200, "query success", users)
                );
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(400, "not found", new ArrayList())
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(400, "something went wrong", null)
            );
        }
    }

    @Override
    public ResponseEntity<ResponseCommon> login(LoginRequest loginRequest) {
        try {
            Boolean isExistedUsername = userDAO.existsByUsername(loginRequest.getUsername());
            if (!isExistedUsername) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                        new ResponseCommon(400, "User doesn't exist", null)
                );
            }
            int userNative = userDAO.loginWithUsernameAndPassowrd(loginRequest.getUsername(), loginRequest.getPassword());
            if (userNative > 0) {
                HashMap<String, String> response = new HashMap<>();
                response.put("token", "ekjrhtkjwhebgrk,hjevrkjhert");
                response.put("expiredTime", String.valueOf(new Date()));
                response.put("refreshToken", "wkehrlkjwhekrjwherk");
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseCommon(200, "Login success: ", response)
                );
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                    new ResponseCommon(200, "Password is not correct", null)
            );

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseCommon(500, "Internal server error: ", null)
            );
        }
    }

    @Override
    public ResponseEntity<ResponseCommon> getStudentList() {
        List<StudentModels> students = studentDAO.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseCommon(200, "success", students));
    }

    @Override
    public ResponseEntity<ResponseCommon> getDetailStudent(Long id) {
        Optional<StudentModels> student = studentDAO.findById(id);
        if (student.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ResponseCommon(200, "query success", student));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseCommon(200, "not found", null));
    }

    @Override
    public ResponseEntity<ResponseCommon> getTeacherList() {
        List<TeacherModels> teachers = teacherDAO.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseCommon(200, "success", teachers));
    }

    @Override
    public ResponseEntity<ResponseCommon> getDetailTeacher(Long id) {
        Optional<TeacherModels> teacher = teacherDAO.findById(id);
        if (teacher.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ResponseCommon(200, "query success", teacher));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseCommon(200, "not found", null));
    }

    @Override
    public ResponseEntity<ResponseCommon> editUser(UserRequest requestBody, Long id) {
        try {
            Optional<UserModels> user = userDAO.findById(id);
            if (user.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseCommon(400, "not found user", null)
                );
            }
            if (!user.get().getType().equals(requestBody.getType())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseCommon(400, "could not change " + user.get().getType() + "to " + requestBody.getType(), null)
                );
            }
            if (!user.get().getUsername().equals(requestBody.getUsername())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseCommon(400, "could not change username" , null)
                );
            }

            Date date = user.get().getCreateAt();
            UserModels updateUser = new UserModels();
            updateUser.setEmail(requestBody.getEmail());
            updateUser.setUsername(requestBody.getUsername());
            updateUser.setId(id);
            updateUser.setPassword(requestBody.getPassword());
            updateUser.setPhoneNumber(requestBody.getPhoneNumber());
            updateUser.setFullName(requestBody.getFullName());
            updateUser.setCreateAt(date);
            updateUser.setType(requestBody.getType());
            if (requestBody.getType().matches("student")) {
                StudentModels student = updateStudentModels(requestBody, date, id, studentDAO);
                if (student == null) {
                    ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Data cheated, cannot update", null));
                }
                studentDAO.save(student);
            } else {
                TeacherModels teacher = updateTeacherModels(requestBody, date, id, teacherDAO);
                if (teacher == null) {
                    ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Data cheated, cannot update", null));
                }
                teacherDAO.save(teacher);
            }
            userDAO.save(updateUser);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseCommon(200, "update user success", user)
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseCommon(400, "Something went wrong", null));
        }
    }

    @Override
    public ResponseEntity<ResponseCommon> deleteUser(Long id) {
        Optional<UserModels> user = userDAO.findById(id);
        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseCommon(400, "not found user", null));
        }
        /// check student or teacher
        if (user.get().getType().equals("student")) {
            studentDAO.deleteById(id);
        } else {
            teacherDAO.deleteById(id);
        }
        userDAO.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseCommon(200, "Delete success user id: " + id, null)
        );
    }


    private static TeacherModels updateTeacherModels(UserRequest requestBody, Date date, Long id, TeacherDAO teacherDAO) {
        Optional<TeacherModels> getTeacher = teacherDAO.getTeacherByUserId(id);
        if (getTeacher.isEmpty()) {
            return null;
        }
        TeacherModels teacher = new TeacherModels();
        teacher.setType("teacher");
        teacher.setEmail(requestBody.getEmail());
        teacher.setUsername(requestBody.getUsername());
        teacher.setPassword(requestBody.getPassword());
        teacher.setPhoneNumber(requestBody.getPhoneNumber());
        teacher.setFullName(requestBody.getFullName());
        teacher.setCreateAt(date);
        teacher.setUserId(Long.valueOf(id));
        teacher.setId(getTeacher.get().getId());
        return teacher;
    }

    private static StudentModels updateStudentModels(UserRequest requestBody, Date date, Long id, StudentDAO studentDAO) {
        Optional<StudentModels> getStudent = studentDAO.getStudentByUserId(id);
        if (getStudent.isEmpty()) {
            return null;
        }
        StudentModels student = new StudentModels();
        student.setType("student");
        student.setEmail(requestBody.getEmail());
        student.setUsername(requestBody.getUsername());
        student.setPassword(requestBody.getPassword());
        student.setPhoneNumber(requestBody.getPhoneNumber());
        student.setFullName(requestBody.getFullName());
        student.setCreateAt(date);
        student.setUserId(id);
        return student;
    }
}
